import sqlite3
from email_validator import validate_email, EmailNotValidError

conn = sqlite3.connect('users.db')
c = conn.cursor()

"""Creates the table for the database"""
c.execute('''CREATE TABLE IF NOT EXISTS Users
                     (id int NOT NULL PRIMARY KEY, name text, surname text, 
                     email text, phone_1 text, phone_2 text, age int)''')

# Insert new row of data
'''
c.execute("INSERT INTO Users VALUES (1,'Bob','Kean','bobkean@hotmail.com',"
          "'+689064398','+918438954',45)")
c.execute("INSERT INTO Users VALUES (2,'Rob','Kelly','robkelly@hotmail.com',"
          "'+628395671','+917596485',40)")
'''


class EmailTest:
    """
    Email Validator, the conditions for updating are the ones specified in the
    validate_email function
    """

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?'

    def __get__(self, instance, owner):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value: str):
        """
        :param instance:
        :param value:
        :return: email updated
        """

        try:
            valid = validate_email(value)
            instance._email = valid.email
            conn.execute(self.store, [value, instance.key])
            conn.commit()

        except EmailNotValidError as a:
            # email in not valid, an error is printed
            print(str(a))


class PhoneTest:
    """
    Phone Validator, the conditions for updating the phone are:
        - The length must be between 10 and 12 characters
        - It must be an string which first position is a '+'
        - It can only contain numbers
    """

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?'

    def __get__(self, instance, owner):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value: str):
        """
        :param instance:
        :param value:
        :return: phone updated
        """
        minvalue = 10
        maxvalue = 12
        cont = 0
        if not isinstance(value, str):
            raise ValueError(f'Expected {value!r} to be an str')
        if minvalue <= len(value) <= maxvalue:
            if value[0] == "+":
                for i in value:
                    if i in "+123456789":
                        cont += 1
                    else:
                        raise ValueError(f'Expected integers not letters or '
                                         'special characters')
                    if cont == len(value):
                        conn.execute(self.store, [value, instance.key])
                        conn.commit()

            else:
                raise ValueError(f'First Position expected to be "+"')
        else:
            raise ValueError(f'Expected {value!r} to be between'
                             '{self.minvalue!r} and {self.maxvalue!r}')


class AgeTest:
    """
    Age Validator, the conditions for updating the age are:
        - It must be an integer
        - It must be between 0 and 120 years
    """

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?'

    def __get__(self, instance, owner):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value: int):
        """
        :param instance:
        :param value:
        :return: age updated
        """
        if isinstance(value, int):
            if 0 < value < 120:
                conn.execute(self.store, [value, instance.key])
                conn.commit()
            else:
                raise ValueError("{value!r} out of range")
        else:
            raise ValueError("Expected {value!r} to be an integer")


class Table:
    """
    This class is used to assign the values from the sql database to those
    attributes that do not need any validations
    """
    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?'

    def __get__(self, instance, owner):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value):
        conn.execute(self.store, [value, instance.key])
        conn.commit()


class User:
    """
    This class is used to store the different users in the sql database
    """
    table = 'Users'
    key = 'id'
    name = Table()
    surname = Table()
    email = EmailTest()  # Descriptors to validate attributes
    phone_1 = PhoneTest()
    phone_2 = PhoneTest()
    age = AgeTest()

    def __init__(self, key):
        self.key = key


# ID assignments
bob = User(1)
rob = User(2)

# Test
try:
    bob.phone_2 = 850389857
except ValueError:
    print(f'Error when trying to assign the phone')

try:
    bob.phone_2 = "+643953.56"
except ValueError:
    print(f'Error when trying to assign the phone')

try:
    rob.email = "robkelly@gmail"
except EmailNotValidError as e:
    # email in not valid, an error is printed
    print(str(e))

c.execute("SELECT * FROM Users")
print(c.fetchall())
conn.commit()
conn.close()
