"""
Entrega Practica 1 - Lucas Pedroso Heras - 3ºA BUSINESS ANALYTICS E INGENIERÍA INFORMÁTICA - Programación II
"""


class Order:
    """
        La clase Order representa a un pedido el cual tiene unas determinadas características que son sun atributos
        definidos el método __init__
    """
    def __init__(self, id_order: str, customer: str, raw_material: str, min_quality_level: int, min_height: float,
                 max_height: float, min_quantity: int, max_quantity: int, min_cut_length: float, max_cut_length: float):
        self.id_order = id_order
        self.customer = customer
        self.raw_material = raw_material
        self.min_quality_level = min_quality_level
        self.min_height = min_height
        self.max_height = max_height
        self.min_quantity = min_quantity
        self.max_quantity = max_quantity
        self.min_cut_length = min_cut_length
        self.max_cut_length = max_cut_length

        self.assigned_stock_pieces = {}

        """
                                :param ir_order: cadena identificadora de la orden
                                :param customer:identificador del cliente que ha realizado la orden
                                :param raw_material: material usado
                                :param min_quality_level: nivel minimo de calidad requerido
                                :param min_heigt: nivel minimo de altura
                                :param max_heigt: nivel maximo de altura
                                :param min_quantity: nivel minimo de cantidad
                                :param max_quantity: nivel maximo de cantidad
                                :param min_cut_length: nivel minimo de tamaño de cada orden
                                :param max_cut_length: rivel maximo de tamaño de cada orden
                                :param assigned_stock_pieces: diccionario en el que se van a incluir las stockpieces 
                                        asignadas
                    """

    def get_assigned_quantity(self) -> float:
        """Calculo del area total asignado de cada orden
        :param self: un objeto de la clase Order
        :return: sum([sp.get_area() for sp in self.assigned_stock_pieces.values]
        """
        return sum([sp.get_area() for sp in self.assigned_stock_pieces.values])

    def calc_delay(self) -> float:
        raise NotImplementedError


class StockPiece:
    """
    La clase StockPiece hace referencia a una porcion correspondiente a un mapa de una fabricsheet y cuyos atributos
    correspondientes son explicados en el método __init__
    """

    def __init__(self, id_stock_piece: str, height: float, length: float, quality_level: int, position_x: float,
                 position_y: float, id_father_stock_piece: int):
        self.id_stock_piece = id_stock_piece
        self.height = height
        self.length = length
        self.quality_level = quality_level
        self.position_x = position_x
        self.position_y = position_y
        self.id_father_stock_piece = id_father_stock_piece

        self.assigned_order = 0
        self.assigned_maps = 0

    """
                        :param id_stock_piece: Cadena identificadora del stockpiece
                        :param height: altura de la pieza
                        :param length: longitud de la pieza
                        :param quality_level: nivel de calidad de la pieza
                        :param position_x: posición x empezando por la esquina superior izquierda
                        :param position_y: posición y empezando por la esquina superior izquierda
                        :param id_father_stock_piece: referencia a la stockpiece original
                        :param assigned_order: orden asignada
                        :param assigned_maps: mapa asignado
            """
    def get_area(self) -> float:
        """Devuelve el area del stockpiece correspondiente al multiplicar la altura por la longitud
        :param self: un objeto de la clase StockPiece
        :return: self.height * self.length"""
        return self.height * self.length

    def checkresidual(self) -> float:
        raise NotImplementedError

    def assign_order(self, order: Order):
        """Mediante esta función asignamos a una orden varias stockpieces, y para mantener la relacion asignamos a la
        stockpiece la orden correspondiente
        :param order: un objeto de la clase Order
        :returns: self.assigned_order, order.assigned_stock_pieces[order.id_order]"""
        self.assigned_order = order
        order.assigned_stock_pieces[order.id_order] = self
        return self.assigned_order, order.assigned_stock_pieces[order.id_order]


class Map:
    """
    La clase Map es la representacion de los tres tipos de mapas de una fabricsheet.
    Los atributos que definen esta clase son explicados en el metodo __init__
    """
    def __init__(self, maptype: str, real: int):
        self.maptype = maptype
        self.real = real

        self.assigned_stockpieces = set([])
        self.assigned_fabric_sheet = set([])

    """
                    :param maptype: tipo de mapa
                    :param real: booleano que especifica si el mapa es real o una referencia
                    :param assigned_stockpiece: lista que incluye los stockpieces asignados
                    :param assigned_stockpiece: lista que incluye los fabricsheet asignados
    """

    def get_assigned_quantity(self) -> float:
        """Calculo del area total asignado de cada mapa
        :param self: objeto de la clase StockPiece
        :return: sum([sp.get_area() for sp in self.assigned_stockpieces]
               """
        return sum([sp.get_area() for sp in self.assigned_stockpieces])

    def calc_residual_area(self):
        raise NotImplementedError

    def add_stock_piece(self, sp: StockPiece):
        """Mediante esta función asignamos a una mapa varias stockpieces, y para mantener la relacion asignamos a la
                stockpiece el mapa correspondiente
        :param sp: un objeto de la clase StockPiece
        :returns: self.assigned_stockpieces.add(sp), sp.assigned_maps"""
        sp.assigned_maps = self
        return self.assigned_stockpieces.add(sp), sp.assigned_maps

    def delete_stock_piece(self, idStockPiece: int):
        raise NotImplementedError


class FabricSheet:
    """
    La clase FabricSheet representa una unica fabric sheet con sus atributos. Y cuyas características son explicadas
    en el método __init__
    """
    def __init__(self, id_fabric_sheet: str, raw_material: str, height: float, length: float, department: str,
                 weaving_forecast: int):
        self.id_fabric_sheet = id_fabric_sheet
        self.raw_material = raw_material
        self.height = height
        self.length = length
        self.department = department
        self.weaving_forecast = weaving_forecast

        self._maps = {}

        self._maps['initial'] = Map('Initial', 1)
        self._maps['intermediate'] = Map('Intermediate', 1)
        self._maps['final'] = Map('Final', 1)

        """
                                :param id_fabric_sheet: Cadena identificadora de fabricsheet
                                :param raw_material: material del que se compone
                                :param height: altura
                                :param length: longitud
                                :param department: localizacion
                                :param weaving_forecast: naturaleza
                    """

    def select_map(self, map1: Map):
        raise NotImplementedError

    def add_map(self, map1: Map):
        """
        Metodo que asigna un mapa al fabric sheet y viceversa, para mantener la relacion
        :param map1: un objeto de la clase Mapa
        :return: self._maps[self.id_fabric_sheet], map1.assigned_fabric_sheet.add(self)
        """
        self._maps["self.id_fabric_sheet"] = map1
        return self._maps["self.id_fabric_sheet"], map1.assigned_fabric_sheet.add(self)


order_1 = Order("001", "Cliente_1", "Lana", 4, 4.5, 3.7, 8, 5, 3.1, 6.0)
order_2 = Order("002", "Cliente_2", "Lana", 8, 3.6, 7.4, 1, 6, 1.1, 6.0)


sp1 = StockPiece("1", 3, 4, 6, 2, 2, 3)
sp2 = StockPiece("2", 2, 3, 8, 3, 2, 3)

fb_sheet_1 = FabricSheet("001", "Lana", 6.4, 4.6, "DEP1", 1)

sp1.assign_order(order_1)
sp2.assign_order(order_1)

order_1.get_assigned_quantity()
